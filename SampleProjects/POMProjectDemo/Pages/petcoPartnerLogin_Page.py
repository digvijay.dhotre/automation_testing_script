class PetcopartnerPage():

    def __init__(self, driver):
        self.driver = driver

        self.petcopartnerlogin_link_xpath = "//button[@class='ant-btn ant-btn-link']"
        self.petcousername_textbox_id = "username"
        self.petco_continue_btn_xpath = "//button[@class='sc-hwwEjo hpNebt sc-kpOJdX bOjJIQ']"
        self.petcopassword_textbox_id = "password"
        self.petco_continue_pass_btn_xpath = "//button[@class='sc-hwwEjo hpNebt sc-kpOJdX bOjJIQ']"
        self.petco_2factor_athontication = "//span[contains(text(),'Skip For Now')]"



    def check_petcopartner_Login_link(self):
        self.driver.find_element_by_xpath(self.petcopartnerlogin_link_xpath).click()


    def check_petcouser_Login_valid_username(self, Petcousername):

        self.driver.find_element_by_id(self.petcousername_textbox_id).clear()
        self.driver.find_element_by_id(self.petcousername_textbox_id).send_keys(Petcousername)

    def click_petcoc_countinue_login_btn(self):
        self.driver.find_element_by_xpath(self.petco_continue_btn_xpath).click()

    def check_petcouser_Login_valid_password(self, Petcopass):

        self.driver.find_element_by_id(self.petcopassword_textbox_id).clear()
        self.driver.find_element_by_id(self.petcopassword_textbox_id).send_keys(Petcopass)

    def click_petcoc_countinue_pass_btn(self):
        self.driver.find_element_by_xpath(self.petco_continue_pass_btn_xpath).click()

    def click_2factor_athontication_btn(self):
        self.driver.find_element_by_xpath(self.petco_2factor_athontication).click()