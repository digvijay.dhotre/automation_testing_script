from selenium import webdriver


class ForgotPass():

    def __init__(self, driver):
        self.driver = driver

        self.forgotpass_xpath = "//a[@class='login-form-forgot-left']"
        self.forgot_pass_username_id = "forgot_password_username"
        self.forgotpass_submit_username_xpath="//button[@class='ant-btn login-form-button ant-btn-primary ant-btn-lg']"

    def click_forgotpass_link(self):
        self.driver.find_element_by_xpath(self.forgotpass_xpath).click()

    def enter_forgot_username(self, ForgotUsername):
        self.driver.find_element_by_id(self.forgot_pass_username_id).clear()
        self.driver.find_element_by_id(self.forgot_pass_username_id).send_keys(ForgotUsername)

    def click_forgotpass_usernamebtn(self):
        self.driver.find_element_by_xpath(self.forgotpass_submit_username_xpath).click()