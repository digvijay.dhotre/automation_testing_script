
class ManageUserPage():

    def __init__(self, driver):
        self.driver = driver

        self.Access_Management_xpath = "//span[contains(text(),'Access Management')]"
        self.Manage_Users_xpath = "//a[contains(text(),'Manage Users')]"
        #self.enter_domain_nameornumber_txtfield_click_xpath = ""
        self.enter_domain_nameornumber_txtfield_xpath = "//input[@class='ant-input ant-input-lg']"

        self.select_dropdownmenu_lict_xpath = "//li[contains(text(),'27 - TEST DOMAIN')]"
        self.get_users_button = "//button[@class='ant-btn ant-btn-primary']"
        self.get_firstvalue_from_intable = "//tbody/tr[1]/td[2]/div[1]/a[1]"

    def check_access_mgmt_tab(self):
        self.driver.find_element_by_xpath(self.Access_Management_xpath).click()

    def click_manageuser_subtab(self):
        self.driver.find_element_by_xpath(self.Manage_Users_xpath).click()


    # def enter_domainnumber_click_textbox(self):
    #
    #    self.driver.find_element_by_id(self.enter_domain_nameornumber_txtfield_click_xpath).click()

    def enter_domainnumber_textbox(self, Domnum):
       Webelement = (self.enter_domain_nameornumber_txtfield_xpath).send_keys(Domnum)

    def select_search_item_fromdropdown(self):
        self.driver.find_element_by_xpath(self.select_dropdownmenu_lict_xpath).click()

    def click_getuser_btn(self):
        self.driver.find_element_by_xpath(self.get_users_button).click()

    def check_value_Intable(self):

        self.driver.find_element_by_xpath(self.get_firstvalue_from_intable)

