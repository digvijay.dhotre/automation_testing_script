class InvoicePage():

    def __init__(self, driver):
        self.driver = driver

        self.Invice_mainmenu_xpath = "//span[contains(text(),'Invoice')]"

        # ------------------------------All Invoice --------------------------------------------------------------------------

        self.allinvoice_submenu_xpath = "//a[contains(text(),'All Invoices')]"

        self.search_Invref_or_Invoice_id = "invoice_Search_searchText"
        self.enter_domain_nameornumber_txtfield_xpath = "//body/div[@id='root']/div[1]/section[1]/section[1]/main[1]/section[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]/span[1]"
        self.select_dropdownmenu_lict_xpath = "//li[contains(text(),'27 - TEST DOMAIN')]"
        self.get_users_button = "//button[@class='ant-btn ant-btn-primary']"
        self.get_firstvalue_from_intable = "//tbody/tr[1]/td[2]/div[1]/a[1]"

#------------------------------Submit Invoice --------------------------------------------------------------------------
        # self.Submitinvice_xpath = "//a[contains(text(),'Submit Invoice')]"

    def check_invice_tab(self):
        self.driver.find_element_by_xpath(self.Invice_mainmenu_xpath).click()

    def click_allinvice_subtab(self):
        self.driver.find_element_by_xpath(self.allinvoice_submenu_xpath).click()

    def click_search_Inv_field(self):
        self.driver.find_element_by_xpath(self.search_Invref_or_Invoice_id).click()

    def click_submitInvice_subtab(self):
        self.driver.find_element_by_xpath(self.Submitinvice_xpath).click()
