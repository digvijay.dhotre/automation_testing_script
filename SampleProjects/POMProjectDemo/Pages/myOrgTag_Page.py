class Organize:

    def __init__(self, driver):
        self.driver = driver
        self.My_Organization_tabxpath = "//span[contains(text(),'My Organization')]"

        self.Org_Profile_xpath = "//a[contains(text(),'Org Profile')]"
        self.Manage_org_user_xpath = "//a[contains(text(),'Manage Org Users')]"
        self.Adduser_orguser_mgmt = "//button[@class='ant-btn ant-btn-primary']"
        self.First_name_id = "add_user_form_first_name"
        self.Last_name_id = "add_user_form_last_name"
        self.Email_id = "add_user_form_username"
        self.permission_xpath = "//div[contains(text(),'Select permission')]"
        self.select_dropdown_value = "//li[contains(text(),'Expense End User (Vendor)')]"

    def click_My_Organization_main_tab(self):
        self.driver.find_element_by_xpath(self.OMNI_Enrichment_xpath).click()

    def check_Org_Profile_tab(self):
        self.driver.find_element_by_xpath(self.Product_mainmenu_xpath).click()

    def click_Manage_org_user_sub_tab(self):
        self.driver.find_element_by_xpath(self.OMNI_Enrichment_xpath).click()

    def enter_username(self, username):
        self.driver.find_element_by_id(self.usename_textbox_id).clear()
        uname = self.driver.find_element_by_id(self.usename_textbox_id).send_keys(username)
        return uname

    def enter_password(self, password):
        self.driver.find_element_by_id(self.password_textbox_id).clear
        password1 = self.driver.find_element_by_id(self.password_textbox_id).send_keys(password)
        return password1

    def click_login(self):
        self.driver.find_element_by_xpath(self.submit_but).click()

    def check_invalid_username_message(self):
        self.driver.find_element_by_xpath(self.invalidUsername_message_xpath).text()
