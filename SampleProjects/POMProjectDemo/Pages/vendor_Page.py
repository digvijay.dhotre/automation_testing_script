
class VendorPage():

    def __init__(self, driver):
        self.driver = driver

        self.Access_Management_xpath = "//span[contains(text(),'Access Management')]"
        self.Vendors_xpath = "//*[@id='Access Management$Menu']/li[5]/a"

        self.Vendor_Statusdropdown_xpath = "//*[@id='Vendor_Search_vendorFlag']"

        self.select_active_dropdown = "(//li[contains(text(),'Active')])[1]"

        self.click_onSearch_button_xpath = "//button[@id='vendorSearchFormSearchButton']"

        self.click_onVendor_Id_Name = "//div[@class='ant-select-search__field__wrap']"

        self.click_on_index_0value = "(//ul[@class='ant-select-dropdown-menu  ant-select-dropdown-menu-root ant-select-dropdown-menu-vertical'])[2]"


    def check_access_mgmt_tab(self):
        self.driver.find_element_by_xpath(self.Access_Management_xpath).click()

    def click_vendors_subtab(self):
        self.driver.find_element_by_xpath(self.Vendors_xpath).click()

    def click_vendorstatus(self):
        self.driver.find_element_by_xpath(self.Vendor_Statusdropdown_xpath).click()

    def click_onActive_tab(self):
        self.driver.find_element_by_xpath(self.select_active_dropdown).click()

    def click_onSearch_button(self):
        self.driver.find_element_by_xpath(self.click_onSearch_button_xpath).click()

    def click_onVendor_id_Name(self, Vendorid_name):

        self.driver.find_element_by_id(self.click_onVendor_Id_Name).clear()
        self.driver.find_element_by_id(self.click_onVendor_Id_Name).send_keys(Vendorid_name)

    def click_on_firstvalue_inlist(self):
        self.driver.find_element_by_xpath(self.click_on_index_0value).click()