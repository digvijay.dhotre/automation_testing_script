class Homepage():

    def __init__(self, driver):
        self.driver = driver

        self.Username_link = "//*[@id='root']/div/section/section/header/div/div[2]/span/span[1]/span"
        self.logout_link = "/html/body/div[2]/div/div/ul/li[4]"

    # action = ActionChains(driver)
    # time.sleep(2)
    # parent_level_menu = driver.find_element_by_xpath("//*[@id='root']/div/section/section/header/div/div[2]/span/span[1]/span")
    # action.move_to_element(parent_level_menu).perform()
    ## time.sleep(2)
    # child_level_menu = driver.find_element_by_xpath("/html/body/div[2]/div/div/ul/li[4]")  # logout button click
    # child_level_menu.click()

    def click_username(self):
        self.driver.find_element_by_xpath(self.Username_link).click()

    def click_logout(self):
        self.driver.find_element_by_xpath(self.logout_link).click()
