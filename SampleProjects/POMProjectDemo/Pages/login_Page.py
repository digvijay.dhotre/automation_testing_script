from SampleProjects.POMProjectDemo.Config.Locators import Locators
class LoginPage:
    #these are the By locators - Objects repo.
    def __init__(self, driver):
        self.driver = driver
        self.usename_textbox_id = Locators.usename_textbox_id
        self.password_textbox_id = "normal_login_password"
        self.submit_but = "//button[@type='submit']"
        self.invalidUsername_message_xpath = "//*[@id='root']/div/div/div/div[2]/div/div/div/div/div/div/div/div[2]/div/div/div/span[1]/span"

# these are my page actions
    def enter_username(self, username):
        self.driver.find_element_by_id(self.usename_textbox_id).clear()
        uname = self.driver.find_element_by_id(self.usename_textbox_id).send_keys(username)
        return uname

    def enter_password(self, password):
        self.driver.find_element_by_id(self.password_textbox_id).clear
        password1 = self.driver.find_element_by_id(self.password_textbox_id).send_keys(password)
        return password1

    def click_login(self):
        self.driver.find_element_by_xpath(self.submit_but).click()

    def check_invalid_username_message(self):
        self.driver.find_element_by_xpath(self.invalidUsername_message_xpath).text()



