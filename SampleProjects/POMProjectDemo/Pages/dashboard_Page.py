class DashboardPage():

    def __init__(self, driver):
        self.driver = driver

        self.Dashboard_xpath = "//span[contains(text(),'Dashboards')]"
        self.Overview_maintab_xpath = "(//span[contains(text(),'Overview')])[1]"
        self.Overview_subtab_xpath = "//*[@id='Overview$Menu']/li[1]"

        self.sub_GlossaryTab_xpath = "//*[@id='Overview$Menu']/li[2]"
            #"(//span[@class='nav-text'])[5]"

        # self.select_dropdownmenu_lict_xpath = "//li[contains(text(),'27 - TEST DOMAIN')]"
        # self.get_users_button = "//button[@class='ant-btn ant-btn-primary']"
        # self.get_firstvalue_from_intable = "//tbody/tr[1]/td[2]/div[1]/a[1]"

    def check_Dashboard_tab(self):
        self.driver.find_element_by_xpath(self.Dashboard_xpath).click()

    def click_Overview_maintab(self):
        self.driver.find_element_by_xpath(self.Overview_maintab_xpath).click()

    def click_Overview_subtab(self):
        self.driver.find_element_by_xpath(self.Overview_subtab_xpath).click()

    def click_Subtab_glossary(self):
        self.driver.find_element_by_xpath(self.sub_GlossaryTab_xpath).click()