from selenium.webdriver.common.by import By

from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage


class ProductPage():



    def __init__(self, driver):
        self.driver = driver
        #self.Access_Management_xpath = "//span[contains(text(),'Access Management')]... (//span[contains(text(),'Product')])[1]"
        self.Product_mainmenu_xpath = "//span[contains(text(),'Product')]"
        self.Create_Product_xpath = "// a[contains(text(), 'Create Product')]"
        self.Click_Upload_file_Xpath = "//button[@class='ant-btn ant-btn-primary']"
        self.Click_upload_file_name_id = "create_bulkItem_fileName"
        self.Clickon_choosefile_xpath = "(//button[@class='ant-btn'])[2]"
        self.clickon_submit_button_xpath = "(//button[@class='ant-btn ant-btn-primary'])[2]"



        self.OMNI_Enrichment_xpath = "//a[contains(text(),'Ready for OMNI Enrichment')]"
        self.select_Taxonomydropdown_xpath = "//div[contains(text(),'Select Taxonomy')]"
        # //div[contains(text(),'Select Taxonomy')]"
        self.select_field_fromdropdown_xpath = "//li[contains(text(),'Apparel|Bodywear')]"

        self.click_searchbutton_xpath = "//button[@type='submit']"
        self.omni_Downloadtemplete_button_xpath = "(//button[@class='ant-btn ant-btn-primary'])[2]"
        self.omni_Uploadtemplete_button_xpath = "(//button[@class='ant-btn ant-btn-primary'])[3]"

        self.Taxonomy_on_uploadtemplete_xpath = "(//span[@class='ant-form-item-children'])[4]"
        self.uploadname_on_uploadtemplete_id = "upload_template_form_fileName"
        self.choosefiel_upload_button_xpath = "//div[@class='ant-upload ant-upload-select ant-upload-select-text']"
        self.submitbutton_uploadtemplete = "(//button[@class='ant-btn ant-btn-primary'])[4]"



    def check_product_tab(self):
        self.driver.find_element_by_xpath(self.Product_mainmenu_xpath).click()

    def click_CreateProduct_subtab(self):
        self.driver.find_element_by_xpath(self.Create_Product_xpath).click()

    def click_Upload_file_btn(self):
        self.driver.find_element_by_xpath(self.Click_Upload_file_Xpath).click()

    def click_Upload_file_name_textbox(self, ufilename):
        self.driver.find_element_by_id(self.Click_upload_file_name_id).click()
        self.driver.find_element_by_id(self.Click_upload_file_name_id).send_keys(ufilename)

    def click_Choose_a_file_btn(self):
        self.driver.find_element_by_xpath(self.Clickon_choosefile_xpath).click()

    def Open_Window_And_Choose_file(self, file):
       # self.driver.find_element_by_xpath(self.Clickon_choosefile_xpath).click()
        element = self.driver.find_element(By.XPATH, self.Clickon_choosefile_xpath).click()
        element.send_keys(file).click()


    def click_Submit_btn(self):
        self.driver.find_element_by_xpath(self.clickon_submit_button_xpath).click()



    def click_ReadyforOMNIenrich_subtab(self):
        self.driver.find_element_by_xpath(self.OMNI_Enrichment_xpath).click()

    def click_Taxonomydropdown(self):

        self.driver.find_element_by_xpath(self.select_Taxonomydropdown_xpath).click()

        #select = select_Taxonomydropdown_xpath = "//div[contains(text(),'Select Taxonomy')]"

    def select_dropdown_Taxonomyvalue(self):
        # select = self.select_field_fromdropdown_xpath.click()
        # dropdownvalue = select.select_by_index(0)
        # print("selected from dropdown value", dropdownvalue)
        # return dropdownvalue
        self.driver.find_element_by_xpath(self.select_field_fromdropdown_xpath).click()

    def clickon_searchbutton(self):
        self.driver.find_element_by_xpath(self.click_searchbutton_xpath).click()

    def check_downloadtemplete_button(self):
        self.driver.find_element_by_xpath(self.omni_Downloadtemplete_button_xpath).click()


    def check_uploadtemplete_button(self):
        self.driver.find_element_by_xpath(self.omni_Uploadtemplete_button_xpath).click()

    def check_Taxonomy_field(self):
        self.driver.find_element_by_xpath(self.Taxonomy_on_uploadtemplete_xpath).click()

    def check_uploadname_uploadtemplate_popup(self, uploadfilename):
        self.driver.find_element_by_id(self.uploadname_on_uploadtemplete_id).clear()
        self.driver.find_element_by_id(self.uploadname_on_uploadtemplete_id).send_keys(uploadfilename)

    def select_file_uploadbutton(self):
        self.driver.find_element_by_xpath(self.choosefiel_upload_button_xpath).click()

        file_path = 'D:/'

        self.choosefiel_upload_button_xpath('bulkItem308Template', file_path)


    def clickon_submit_button(self):
        self.driver.find_element_by_xpath(self.Taxonomy_on_uploadtemplete_xpath).click()


