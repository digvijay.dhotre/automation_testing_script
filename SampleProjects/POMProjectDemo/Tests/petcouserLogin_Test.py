import time
import unittest

from selenium import webdriver
import sys
import os

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail

sys.path.append(os.path.join(os.path.dirname(__file__), "...", "..."))
from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage
import HTMLTestRunner


class Petcouser_LoginTest(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="/Users/413047/PycharmProjects/POMplement/Drivers/chromedriver.exe")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_01_Petco_userlogin_valid_user(self):
        driver = self.driver
        driver.get("https://d2mztpwpky9bb5.cloudfront.net/#/")
        print("title is=", driver.title)

        print("login with petco user valid user is")

        time.sleep(6)

        PetcoLoginUser = PetcopartnerPage(driver)
        PetcoLoginUser.check_petcopartner_Login_link()
        driver.implicitly_wait(20)

        PetcoLoginUser.check_petcouser_Login_valid_username(Userdetail.PETCO_USER_NAME)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_login_btn()
        time.sleep(5)

        PetcoLoginUser.check_petcouser_Login_valid_password(Userdetail.PETCO_USER_PASSWORD)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_pass_btn()
        time.sleep(3)

        PetcoLoginUser.click_2factor_athontication_btn()
        print("Petcouser login successfully ")
        time.sleep(20)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ != '__main__':
    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))