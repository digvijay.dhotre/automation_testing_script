import inspect
import time
import unittest
import autoit
from selenium import webdriver
from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail
from SampleProjects.POMProjectDemo.Pages.login_Page import LoginPage
from SampleProjects.POMProjectDemo.Pages.productTab_Page import ProductPage
import HTMLTestRunner

class ProducttabUser(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_01_login_valid_expenseuser(self):
        driver = self.driver
        # driver.get("https://d2mztpwpky9bb5.cloudfront.net/#/")
        driver.get(Userdetail.BASE_URL)

        print("title is=", driver.title)

        login = LoginPage(driver)
        login.enter_username(Userdetail.USER_NAME)  # get varible from config file
        login.enter_password(Userdetail.USER_PASSWORD)
        login.click_login()
        time.sleep(8)
        print("login button clicked")


        print("User is on home page")


    def test_02_Click_CreateProdtab_InproductTab(self):

        driver = self.driver
        producttab = ProductPage(driver)
        time.sleep(3)
        print("user is on home page ")

        producttab.check_product_tab()  # click on product menu tab
        time.sleep(3)

        producttab.click_CreateProduct_subtab()  # click on ReadyforOMNIenrich from product tab
        time.sleep(10)

        producttab.click_Upload_file_btn()  # click on dropdown
        time.sleep(5)

        #producttab.click_Upload_file_name_textbox()
        time.sleep(3)
        producttab.click_Upload_file_name_textbox(Userdetail.UPLOAD_FILE_NAME)

        time.sleep(5)
        producttab.click_Choose_a_file_btn()

        time.sleep(10)


        producttab.Userdetail.FILE_PATH
        time.sleep(5)

        autoit.run()

        producttab.click_Submit_btn()
        time.sleep(5)






    # def test_03_Click_OMINItab_InproductTab(self):
    #
    #     driver = self.driver
    #     producttab = ProductPage(driver)
    #
    #     time.sleep(3)
    #     print("user is on home page ")
    #
    #     producttab.check_product_tab()                #click on product menu tab
    #     time.sleep(3)
    #
    #     producttab.click_ReadyforOMNIenrich_subtab()    #click on ReadyforOMNIenrich from product tab
    #     time.sleep(10)
    #
    #     producttab.click_Taxonomydropdown()     # click on dropdown
    #     time.sleep(5)
    #
    #     producttab.select_dropdown_Taxonomyvalue()   #select taxonomy from dropdown
    #     time.sleep(5)
    #
    #     producttab.clickon_searchbutton()   #search button click
    #     time.sleep(5)
    #
    #     producttab.omni_Downloadtemplete_button_xpath()  # downloadtemeplete button
    #     time.sleep(5)
    #
    #     producttab.omni_Uploadtemplete_button_xpath()    #upload templete button
    #     time.sleep(5)
    #
    #     #producttab.check_Taxonomy_field()
    #    # self.assertEqual(dropdownvalue, "Apparel|Bodywear", "webpage title is not matching")
    #
    #     time.sleep(5)
    #
    #     producttab.check_uploadname_uploadtemplate_popup("digvijayTest1")
    #     time.sleep(5)
    #
    #     producttab.select_file_uploadbutton()
    #
    #     time.sleep(5)


    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")



if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))
