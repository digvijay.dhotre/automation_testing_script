import time
import unittest
from selenium import webdriver
import sys
import os

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail

sys.path.append(os.path.join(os.path.dirname(__file__), "...", "..."))
from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage
from SampleProjects.POMProjectDemo.Pages.manageUser_Page import ManageUserPage
import HTMLTestRunner

class ManageUserTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()


    def test_01_Petcouserlogin_validuser(self):
        driver = self.driver
        driver.get(Userdetail.BASE_URL)
        print("title is=", driver.title)


        print("login with petco user valid user is")

        time.sleep(6)

        PetcoLoginUser = PetcopartnerPage(driver)
        PetcoLoginUser.check_petcopartner_Login_link()
        driver.implicitly_wait(20)

        PetcoLoginUser.check_petcouser_Login_valid_username(Userdetail.PETCO_USER_NAME)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_login_btn()
        time.sleep(5)

        PetcoLoginUser.check_petcouser_Login_valid_password(Userdetail.PETCO_USER_PASSWORD)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_pass_btn()

        print("Petcouser login successfully ")
        time.sleep(20)

    def test_02_ManageUser(self):

        driver = self.driver
        manageuser = ManageUserPage(driver)

        manageuser.check_access_mgmt_tab()
        time.sleep(3)

        manageuser.click_manageuser_subtab()
        time.sleep(5)
        # manageuser.enter_domainnumber_click_textbox()
        # time.sleep(2)
        manageuser.enter_domainnumber_textbox("27")
        time.sleep(3)

        manageuser.select_search_item_fromdropdown()
        time.sleep(3)

        manageuser.click_getuser_btn()
        time.sleep(3)

        manageuser.check_value_Intable()
        try:
         l = driver.find_element_by_xpath("//tbody/tr[1]/td[2]/div[1]/a[1]")
         s = l.text
         print("Element exist in tabel -" + s)

    # NoSuchElementException thrown if not present
        except NoSuchElementException:
         print("Element does not exist")
        self.driver.close()

        # sel = Select(manageuser.select_search_item_fromdropdown)
        # time.sleep(3)
        # # select by select_by_visible_text() method
        # sel.select_by_visible_text("27 - TEST DOMAIN").click()
        # time.sleep(3)



    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ == '__main__':

    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))