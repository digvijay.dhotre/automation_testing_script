import unittest
import time

from selenium import webdriver

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail
from SampleProjects.POMProjectDemo.Pages.invoice_Page import InvoicePage
import HTMLTestRunner

from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage


class InvoicetabUser(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_01_Petcouserlogin_validuser(self):
        driver = self.driver
        driver.get(Userdetail.BASE_URL)
        print("title is=", driver.title)

        print("login with petco user valid user is")

        time.sleep(8)

        PetcoLoginUser = PetcopartnerPage(driver)
        PetcoLoginUser.check_petcopartner_Login_link()
        driver.implicitly_wait(20)

        PetcoLoginUser.check_petcouser_Login_valid_username(Userdetail.PETCO_USER_NAME)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_login_btn()
        time.sleep(5)

        PetcoLoginUser.check_petcouser_Login_valid_password(Userdetail.PETCO_USER_PASSWORD)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_pass_btn()
        time.sleep(5)

        PetcoLoginUser.click_2factor_athontication_btn()
        print("Petcouser login successfully ")
        time.sleep(20)

    def test_02_invoiceAll(self):
        driver = self.driver
        Invoiceob = InvoicePage(driver)
        Invoiceob.check_invice_tab()
        time.sleep(3)

        Invoiceob.click_allinvice_subtab()
        time.sleep(3)

        Invoiceob.click_search_Inv_field()
        time.sleep(3)
        Invoiceob.click_allinvice_subtab()
        time.sleep(3)
        Invoiceob.click_allinvice_subtab()
        time.sleep(3)

        #Invoiceob.click_submitInvice_subtab()
        time.sleep(5)


    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ == '__main__':

    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))