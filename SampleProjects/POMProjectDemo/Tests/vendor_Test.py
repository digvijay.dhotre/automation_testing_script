import time
import unittest
import configparser
from telnetlib import EC

from selenium import webdriver
from selenium.webdriver import ActionChains
import sys
import os

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail
from SampleProjects.POMProjectDemo.Pages.forgotpass_Page import ForgotPass
from SampleProjects.POMProjectDemo.Pages.home_Page import Homepage
from SampleProjects.POMProjectDemo.Pages.login_Page import LoginPage
from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage
from SampleProjects.POMProjectDemo.Pages.vendor_Page import VendorPage

sys.path.append(os.path.join(os.path.dirname(__file__), "...", "..."))
import HTMLTestRunner

class VendorTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()


    def test_01_Petcouserlogin_validuser(self):
        driver = self.driver
        driver.get(Userdetail.BASE_URL)
        print("title is=", driver.title)


        print("login with petco user valid user is")

        time.sleep(6)

        PetcoLoginUser = PetcopartnerPage(driver)
        PetcoLoginUser.check_petcopartner_Login_link()
        driver.implicitly_wait(20)

        PetcoLoginUser.check_petcouser_Login_valid_username(Userdetail.PETCO_USER_NAME)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_login_btn()
        time.sleep(5)

        PetcoLoginUser.check_petcouser_Login_valid_password(Userdetail.PETCO_USER_PASSWORD)
        time.sleep(3)

        PetcoLoginUser.click_petcoc_countinue_pass_btn()
        time.sleep(3)

        PetcoLoginUser.click_2factor_athontication_btn()
        print("Petcouser login successfully ")
        time.sleep(20)


    def test_02_vendors_status(self):

        driver = self.driver
        vendoruser = VendorPage(driver)

        vendoruser.check_access_mgmt_tab()
        time.sleep(3)

        vendoruser.click_vendors_subtab()
        time.sleep(5)

        vendoruser.click_vendorstatus()
        time.sleep(3)

        vendoruser.click_onActive_tab()
        time.sleep(5)

        print("select active button ")
        vendoruser.click_onSearch_button()
        print("click on search button ")
        time.sleep(3)

    def test_03_vendors_status(self):
        driver = self.driver
        vendoruser = VendorPage(driver)

        vendoruser.click_onVendor_id_Name(Userdetail.VENDOR_ID_OR_NAME)
        time.sleep(3)

        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((vendoruser.click_on_index_0value, "1234 - CLARKWOOD SHOPPING CENTER"))
        )
        # click the element
        element.click()
        time.sleep(3)

        vendoruser.click_onSearch_button()
        print("click on search button ")


    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ == '__main__':

    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))