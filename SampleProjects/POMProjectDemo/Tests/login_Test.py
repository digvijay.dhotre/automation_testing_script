import time
import unittest
import configparser
from selenium import webdriver
from selenium.webdriver import ActionChains
import sys
import os

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail
from SampleProjects.POMProjectDemo.Pages.forgotpass_Page import ForgotPass
from SampleProjects.POMProjectDemo.Pages.home_Page import Homepage
from SampleProjects.POMProjectDemo.Pages.login_Page import LoginPage
from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage


sys.path.append(os.path.join(os.path.dirname(__file__), "...", "..."))
import HTMLTestRunner


class Login_Testt(unittest.TestCase):



    @classmethod
    def setUpClass(cls):

        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_01_login_valid(self):

        driver = self.driver
        #driver.get("https://d2mztpwpky9bb5.cloudfront.net/#/")
        driver.get(Userdetail.BASE_URL)

        print("title is=", driver.title)

        login = LoginPage(driver)
        login.enter_username(Userdetail.USER_NAME)    #get varible from config file
        login.enter_password(Userdetail.USER_PASSWORD)
        login.click_login()
        time.sleep(8)
        print("login button clicked")

        print("user clicked on logout button")
        time.sleep(2)

        homepage = Homepage(driver)
        print("user is on home")
        time.sleep(2)
        homepage.click_username()
        print("user clicked on username 2")
        time.sleep(2)
        homepage.click_logout()
        print("user clicked on logout button 2 ")
        # self.driver.find_element_by_xpath("//*[@id='root']/div/section/section/header/div/div[2]/span/span[2]").click
        time.sleep(5)
        print("Click on user name completed")




    # def test_02_login_invalid(self):
    #     driver = self.driver
    #     driver.get(Userdetail.BASE_URL)
    #     print("login with not valid user is")
    #
    #     login = LoginPage(driver)
    #     login.enter_username("digvijay@gmail.com")
    #     time.sleep(2)
    #
    #     login.enter_password("Dig@1449")
    #     time.sleep(2)
    #
    #     login.click_login()
    #     time.sleep(3)
    #
    #     message = driver.find_element_by_xpath("//span[@class='ant-typography']")
    #     # self.assertEqual(message, "User does not exist.")
    #     print(message)
    #     time.sleep(5)
    #
    # def test_03_forgotpass_link(self):
    #     driver = self.driver
    #     driver.get(Userdetail.BASE_URL)
    #     print("login with petco user valid user is")
    #
    #     time.sleep(5)
    #
    #     Forgotpass = ForgotPass(driver)
    #     Forgotpass.click_forgotpass_link()
    #
    #     Forgotpass.enter_forgot_username(Userdetail.USER_NAME)
    #     time.sleep(3)
    #
    #     Forgotpass.click_forgotpass_usernamebtn()
    #     time.sleep(3)
    #
    #     print("mail is send on entered email id", Forgotpass)
    #     time.sleep(8)
    #
    # def test_04_loginwithPetcouser_valid(self):
    #     driver = self.driver
    #     driver.get(Userdetail.BASE_URL)
    #     print("login with petco user valid user is")
    #
    #     time.sleep(10)
    #
    #     PetcoLogin = PetcopartnerPage(driver)
    #     PetcoLogin.check_petcopartner_Login_link()
    #     driver.implicitly_wait(20)
    #
    #     PetcoLogin.check_petcouser_Login_valid_username(Userdetail.PETCO_USER_NAME)
    #     time.sleep(3)
    #
    #     PetcoLogin.click_petcoc_countinue_login_btn()
    #     time.sleep(5)
    #
    #     PetcoLogin.check_petcouser_Login_valid_password(Userdetail.PETCO_USER_PASSWORD)
    #     time.sleep(3)
    #
    #     PetcoLogin.click_petcoc_countinue_pass_btn()
    #     time.sleep(5)
    #     print("Petcouser login successfully ")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ == '__main__':

    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))