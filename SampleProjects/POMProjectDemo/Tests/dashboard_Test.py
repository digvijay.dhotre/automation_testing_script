import time
import unittest
from selenium import webdriver
import sys
import os

from SampleProjects.POMProjectDemo.Config.Config_file import Userdetail
from SampleProjects.POMProjectDemo.Pages.home_Page import Homepage
from SampleProjects.POMProjectDemo.Pages.login_Page import LoginPage

sys.path.append(os.path.join(os.path.dirname(__file__), "...", "..."))
from SampleProjects.POMProjectDemo.Pages.petcoPartnerLogin_Page import PetcopartnerPage
from SampleProjects.POMProjectDemo.Pages.dashboard_Page import DashboardPage
import HTMLTestRunner

class DashboardTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=Userdetail.CHROM_EXECUTABLE_PATH)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_01_login_valid(self):
        driver = self.driver
        driver.get("https://d2nw3eyv56bruo.cloudfront.net/#/")
        #driver.get(Userdetail.BASE_URL)

        print("title is=", driver.title)

        login = LoginPage(driver)
        login.enter_username("testuser1@mailinator.com")  # get varible from config file
        login.enter_password("Pass@1234")
        login.click_login()
        time.sleep(8)
        print("login button clicked")

        print("user clicked on logout button")
        time.sleep(2)

        homepage = Homepage(driver)
        print("user is on home")
        time.sleep(10)

        print("Click on user name completed")

    def test_02_Click_Dashboardtab(self):

        driver = self.driver
        driver.implicitly_wait(10)
        dashboardtab = DashboardPage(driver)

        time.sleep(10)
        print("user is on home page ")

        dashboardtab.check_Dashboard_tab()    #click on dashboard tab
        time.sleep(5)
        print("user is dashboard tab")

        dashboardtab.click_Overview_maintab()    #click on ReadyforOMNIenrich from product tab
        time.sleep(10)
        print("user is overivew tab")

        dashboardtab.click_Overview_subtab()  # click on ReadyforOMNIenrich from product tab
        time.sleep(15)
        print("user is overivew sub tab")

        dashboardtab.click_Subtab_glossary()     # click on dropdown
        time.sleep(10)
        print("user is subtabtab_glossary")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Test completed")


if __name__ == '__main__':

    unittest.main(testRunner=HTMLTestRunner.HTMLTestRunner(output='/Users/413047/PycharmProjects/POMplement/reports'))