import configparser
import unittest
import sys
from selenium import webdriver


class sampleTest(unittest.TestCase):
    # default file name in case no arg passed
    config_file = "config.ini"

    def setUp(self):
        self.config = configparser.RawConfigParser(allow_no_value=True)
        self.config.read(self.config_file)
        username = self.config.get("USER_INFO", "LT_USERNAME")
        access_key = self.config.get("USER_INFO", "LT_ACCESS_KEY")

        ## get CAPABILITIES from https://www.lambdatest.com/capabilities-generator/

        desired_caps = {
            "build": self.config.get("CAPABILITIES", "build"),
            "name": self.config.get("CAPABILITIES", "name"),
            "platform": self.config.get("CAPABILITIES", "platform"),
            "browserName": self.config.get("CAPABILITIES", "browserName"),
            "version": self.config.get("CAPABILITIES", "version"),
            "selenium_version": self.config.get("CAPABILITIES", "selenium_version"),
            "visual": self.config.get("CAPABILITIES", "visual"),
            "geoLocation": self.config.get("CAPABILITIES", "geoLocation"),
            "chrome.driver": self.config.get("CAPABILITIES", "chrome_driver"),
        }

        """
            Setup remote driver
            -------
            username and access_key can be found on lt platform

        """
        self.driver = webdriver.Remote(
            command_executor="https://{}:{}@hub.lambdatest.com/wd/hub".format(
                username, access_key
            ),
            desired_capabilities=desired_caps,
        )

    # tearDown runs after each test case
    def tearDown(self):
        self.driver.quit()

    # """ You can write the test cases here """
    def test_unit_user_should_able_to_add_item(self):
        # try:
        driver = self.driver

        # Url
        driver.get("https://lambdatest.github.io/sample-todo-app/")

        # Click on checkbox
        check_box_one = driver.find_element_by_name("li1")
        check_box_one.click()

        # Click on checkbox
        check_box_two = driver.find_element_by_name("li2")
        check_box_two.click()

        # Enter item in textfield
        textfield = driver.find_element_by_id("sampletodotext")
        textfield.send_keys(self.config.get("ITEM_TO_ADD", "ITEM_VALUE"))

        # Click on add button
        add_button = driver.find_element_by_id("addbutton")
        add_button.click()

        # navigation to list to find added item
        custom_xpath = (
            "//*[contains(text()," + self.config.get("ITEM_TO_ADD", "ITEM_VALUE") + ")]"
        )
        added_item = driver.find_element_by_xpath(custom_xpath).text

        # saving screenshot
        driver.save_screenshot(self.config.get("SCREENSHOT_FILE_INFO", "FILENAME"))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        sampleTest.config_file = sys.argv.pop()
    unittest.main()